export const environment = {
  production: true,
  application: {
    name: 'ArincToolkitPlus',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44334',
    clientId: 'ArincToolkitPlus_App',
    dummyClientSecret: '1q2w3e*',
    scope: 'ArincToolkitPlus',
    showDebugInformation: true,
    oidc: false,
    requireHttps: true,
  },
  apis: {
    default: {
      url: 'https://localhost:44334',
    },
  },
  localization: {
    defaultResourceName: 'ArincToolkitPlus',
  },
};
