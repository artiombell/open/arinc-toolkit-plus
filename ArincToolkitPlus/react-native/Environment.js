const ENV = {
  dev: {
    apiUrl: 'http://localhost:44334',
    oAuthConfig: {
      issuer: 'http://localhost:44334',
      clientId: 'ArincToolkitPlus_App',
      clientSecret: '1q2w3e*',
      scope: 'ArincToolkitPlus',
    },
    localization: {
      defaultResourceName: 'ArincToolkitPlus',
    },
  },
  prod: {
    apiUrl: 'http://localhost:44334',
    oAuthConfig: {
      issuer: 'http://localhost:44334',
      clientId: 'ArincToolkitPlus_App',
      clientSecret: '1q2w3e*',
      scope: 'ArincToolkitPlus',
    },
    localization: {
      defaultResourceName: 'ArincToolkitPlus',
    },
  },
};

export const getEnvVars = () => {
  // eslint-disable-next-line no-undef
  return __DEV__ ? ENV.dev : ENV.prod;
};
