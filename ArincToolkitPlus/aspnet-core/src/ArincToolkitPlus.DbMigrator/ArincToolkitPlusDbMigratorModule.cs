﻿using ArincToolkitPlus.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace ArincToolkitPlus.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(ArincToolkitPlusEntityFrameworkCoreDbMigrationsModule),
        typeof(ArincToolkitPlusApplicationContractsModule)
        )]
    public class ArincToolkitPlusDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
