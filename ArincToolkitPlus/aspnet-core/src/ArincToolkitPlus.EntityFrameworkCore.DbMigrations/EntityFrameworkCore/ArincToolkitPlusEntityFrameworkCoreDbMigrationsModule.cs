﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace ArincToolkitPlus.EntityFrameworkCore
{
    [DependsOn(
        typeof(ArincToolkitPlusEntityFrameworkCoreModule)
        )]
    public class ArincToolkitPlusEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<ArincToolkitPlusMigrationsDbContext>();
        }
    }
}
