﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ArincToolkitPlus.EntityFrameworkCore
{
    /* This class is needed for EF Core console commands
     * (like Add-Migration and Update-Database commands) */
    public class ArincToolkitPlusMigrationsDbContextFactory : IDesignTimeDbContextFactory<ArincToolkitPlusMigrationsDbContext>
    {
        public ArincToolkitPlusMigrationsDbContext CreateDbContext(string[] args)
        {
            ArincToolkitPlusEfCoreEntityExtensionMappings.Configure();

            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<ArincToolkitPlusMigrationsDbContext>()
                .UseSqlServer(configuration.GetConnectionString("Default"));

            return new ArincToolkitPlusMigrationsDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
