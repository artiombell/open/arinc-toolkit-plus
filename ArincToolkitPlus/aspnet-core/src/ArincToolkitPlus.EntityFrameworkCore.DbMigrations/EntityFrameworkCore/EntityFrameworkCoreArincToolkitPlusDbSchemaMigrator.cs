﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ArincToolkitPlus.Data;
using Volo.Abp.DependencyInjection;

namespace ArincToolkitPlus.EntityFrameworkCore
{
    public class EntityFrameworkCoreArincToolkitPlusDbSchemaMigrator
        : IArincToolkitPlusDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreArincToolkitPlusDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the ArincToolkitPlusMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<ArincToolkitPlusMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}