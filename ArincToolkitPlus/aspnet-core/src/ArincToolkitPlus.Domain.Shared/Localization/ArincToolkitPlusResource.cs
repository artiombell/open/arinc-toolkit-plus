﻿using Volo.Abp.Localization;

namespace ArincToolkitPlus.Localization
{
    [LocalizationResourceName("ArincToolkitPlus")]
    public class ArincToolkitPlusResource
    {

    }
}