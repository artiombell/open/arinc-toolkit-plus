﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace ArincToolkitPlus.EntityFrameworkCore
{
    public static class ArincToolkitPlusDbContextModelCreatingExtensions
    {
        public static void ConfigureArincToolkitPlus(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(ArincToolkitPlusConsts.DbTablePrefix + "YourEntities", ArincToolkitPlusConsts.DbSchema);

            //    //...
            //});
        }
    }
}