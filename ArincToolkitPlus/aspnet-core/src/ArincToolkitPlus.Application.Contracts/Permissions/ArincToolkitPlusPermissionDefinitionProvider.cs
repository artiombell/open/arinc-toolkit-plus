﻿using ArincToolkitPlus.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace ArincToolkitPlus.Permissions
{
    public class ArincToolkitPlusPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(ArincToolkitPlusPermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(ArincToolkitPlusPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<ArincToolkitPlusResource>(name);
        }
    }
}
