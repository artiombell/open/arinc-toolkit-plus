﻿using AutoMapper;

namespace ArincToolkitPlus
{
    public class ArincToolkitPlusApplicationAutoMapperProfile : Profile
    {
        public ArincToolkitPlusApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
