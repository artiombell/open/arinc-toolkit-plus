﻿using System;
using System.Collections.Generic;
using System.Text;
using ArincToolkitPlus.Localization;
using Volo.Abp.Application.Services;

namespace ArincToolkitPlus
{
    /* Inherit your application services from this class.
     */
    public abstract class ArincToolkitPlusAppService : ApplicationService
    {
        protected ArincToolkitPlusAppService()
        {
            LocalizationResource = typeof(ArincToolkitPlusResource);
        }
    }
}
