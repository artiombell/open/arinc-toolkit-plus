﻿using ArincToolkitPlus.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace ArincToolkitPlus.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class ArincToolkitPlusController : AbpController
    {
        protected ArincToolkitPlusController()
        {
            LocalizationResource = typeof(ArincToolkitPlusResource);
        }
    }
}