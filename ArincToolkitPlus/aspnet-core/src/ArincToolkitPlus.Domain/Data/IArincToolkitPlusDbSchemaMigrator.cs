﻿using System.Threading.Tasks;

namespace ArincToolkitPlus.Data
{
    public interface IArincToolkitPlusDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
