﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace ArincToolkitPlus.Data
{
    /* This is used if database provider does't define
     * IArincToolkitPlusDbSchemaMigrator implementation.
     */
    public class NullArincToolkitPlusDbSchemaMigrator : IArincToolkitPlusDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}