﻿using Volo.Abp.Modularity;

namespace ArincToolkitPlus
{
    [DependsOn(
        typeof(ArincToolkitPlusApplicationModule),
        typeof(ArincToolkitPlusDomainTestModule)
        )]
    public class ArincToolkitPlusApplicationTestModule : AbpModule
    {

    }
}