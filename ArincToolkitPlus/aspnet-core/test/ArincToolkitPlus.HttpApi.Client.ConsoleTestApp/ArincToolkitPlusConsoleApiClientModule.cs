﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace ArincToolkitPlus.HttpApi.Client.ConsoleTestApp
{
    [DependsOn(
        typeof(ArincToolkitPlusHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class ArincToolkitPlusConsoleApiClientModule : AbpModule
    {
        
    }
}
