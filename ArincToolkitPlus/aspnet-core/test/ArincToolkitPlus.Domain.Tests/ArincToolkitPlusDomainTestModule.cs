﻿using ArincToolkitPlus.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace ArincToolkitPlus
{
    [DependsOn(
        typeof(ArincToolkitPlusEntityFrameworkCoreTestModule)
        )]
    public class ArincToolkitPlusDomainTestModule : AbpModule
    {

    }
}